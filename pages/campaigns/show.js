import React,{Component}from 'react';
import Layout from '../../components/Layout';
import Campaignsss from'../../ethereum/campaign';
import { Card ,Grid,Button} from 'semantic-ui-react';
import web3 from '../../ethereum/web3';
import ContributionForm from '../../components/ContributeForm';
import {Link} from '../../routes';

class Campaignshow extends Component{
    static async getInitialProps(props){
        const campaign = Campaignsss(props.query.address);
        //console.log(props.query.address);//擷取這個網址的url那part的address(from routes??)
        const summary = await campaign.methods.getSummary().call();
        //console.log(summary);
        return {
            address:props.query.address,
            minimumContribution:summary[0],
            balance:summary[1],
            requestsCount:summary[2],
            approversCount:summary[3],
            manager:summary[4]
        };
    }
    renderCards(){
        const{
            balance,
            manager,
            minimumContribution,
            requestsCount,
            approversCount
        } = this.props;
        const items = [
            {
                header:manager,
                meta:'Address of manager',
                description:'the manager create campaign and request to withdraw the money',
                style:{overflowWrap:'break-word'}
            },
            {
                header:minimumContribution,
                meta:'minimumContribution(wei)',
                description:' > this $$'
            },
            {
                header:requestsCount,
                meta:'Number of request',
                description:'khjgfd'
            },
            {
                header:approversCount,
                meta:'number of appovers',
                description:'jlkhjgfdsghjkljkhgfdghjk'
            },
            {
                header:web3.utils.fromWei(balance,'ether'),
                meta:'campaign balance',
                description:'kjhgfdghjk'
            }
        ];

        return<Card.Group items = {items}/>;
    }
    render(){
        return (
            <Layout>
                <h3>Campaignshow</h3>
                <Grid>
                    <Grid.Row>
                    <Grid.Column width = {10}>
                        {this.renderCards()}
                    </Grid.Column>
                    <Grid.Column width = {6}>
                        <ContributionForm address = {this.props.address}/>
                    </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                        <Link route = {`/campaigns/${this.props.address}/requests`}>
                            <a>
                                <Button primary>View Requests</Button>
                            </a>
                        </Link>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>    
            </Layout>
        );
    }
}

export default Campaignshow;