const path = require('path');
const solc = require('solc');
const fs = require('fs-extra');//fs-extra比fs module多一些功能的樣子

const buildPath = path.resolve(__dirname,'build');
fs.removeSync(buildPath);
//就是清掉build裡面的東西包括folder

const campaignPath = path.resolve(__dirname,'contracts','Campaign.sol');
const source = fs.readFileSync(campaignPath,'utf8');
const output = solc.compile(source,1).contracts;

fs.ensureDirSync(buildPath);
//console.log(output);
for(let contract in output){
    fs.outputJSONSync(
        path.resolve(buildPath,contract.replace(':','')+'.json'),
        output[contract]
    );
}