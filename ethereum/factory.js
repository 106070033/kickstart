import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const instance = new web3.eth.Contract(
    JSON.parse(CampaignFactory.interface),
    '0x5f56938E0ce4B7A18F7655B552647463e464ce55'
);

export default instance;