const routes = require('next-routes')();

routes
    .add('/campaigns/new','/campaigns/new')
    .add('/campaigns/:address', '/campaigns/show')//:後面的不管都是跑campaign/show的檔案內容？
    .add('/campaigns/:address/requests','/campaigns/requests/index')
    .add('/campaigns/:address/requests/new','/campaigns/requests/new');

module.exports = routes;