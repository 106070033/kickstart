'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//const web3 = new Web3(window.ethereum);
//window是跟瀏覽器相關的，但是跟next.js不在同一個地方，所以不能access nect.js的server
//因為這個檔案會被跑兩遍，一次給server一次給browser
//window.ethereum.enable();
var web3 = void 0;
if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
    //inside的browser,metamask is available
    // we are in the browser
    web3 = new _web2.default(window.ethereum);
    window.ethereum.enable();
} else {
    // we are on the server *or* ther user is not running the metamask
    var provider = new _web2.default.providers.HttpProvider('https://rinkeby.infura.io/v3/70c54a37736c4698951498533c361e3c');
    web3 = new _web2.default(provider);
}

exports.default = web3;
///這樣就會有新的web3 version
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV0aGVyZXVtL3dlYjMuanMiXSwibmFtZXMiOlsiV2ViMyIsIndlYjMiLCJ3aW5kb3ciLCJldGhlcmV1bSIsImVuYWJsZSIsInByb3ZpZGVyIiwicHJvdmlkZXJzIiwiSHR0cFByb3ZpZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxBQUFPLEFBQVA7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxZQUFKO0FBQ0EsSUFBRyxPQUFPLEFBQVAsV0FBa0IsQUFBbEIsZUFBa0MsT0FBTyxPQUFPLEFBQWQsU0FBdUIsQUFBNUQsYUFBd0UsQUFBQztBQUNyRTtBQUNBO1dBQU8sQUFBSSxBQUFKLGtCQUFTLE9BQU8sQUFBaEIsQUFBUCxBQUNBO1dBQU8sQUFBUCxTQUFnQixBQUFoQixBQUNIO0FBSkQsT0FJSyxBQUNEO0FBQ0E7UUFBTSxXQUFXLElBQUksY0FBSyxBQUFMLFVBQWUsQUFBbkIsYUFDYixBQURhLEFBQWpCLEFBR0E7V0FBTyxBQUFJLEFBQUosa0JBQVMsQUFBVCxBQUFQLEFBQ0g7QUFFRDs7a0JBQWUsQUFBZjtBQUNBIiwiZmlsZSI6IndlYjMuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL3N1dmluY2VudC/lsIjpoYwvd2ludGVyL2tpY2tzdGFydCJ9